//
//  ViewModel.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/28.
//

import Foundation
import UIKit

// ViewModel 類，用於管理 CollectionView 的偏移量和觀察狀態
class ViewModel: NSObject, ObservableObject {
    @Published var collectionView: UICollectionView?
    @Published var offset: CGFloat = 0
    @Published var isObserved = false
    
    deinit {
        remove()
    }
    
    func observe() {
        guard !isObserved else { return }
        collectionView?.addObserver(self, forKeyPath: "contentOffset", context: nil)
        isObserved = true
    }
    
    func remove() {
        isObserved = false
        collectionView?.removeObserver(self, forKeyPath: "contentOffset")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath == "contentOffset" else { return }
        if let contentOffset = (object as? UICollectionView)?.contentOffset {
            offset = contentOffset.x
        }
    }
}
