//
//  SwiftUIView.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/29.
//

import SwiftUI

enum Tabs: Hashable {
    case 團體, 訂房, 自由行, 票券
}

struct MainPage: View {
    @State private var selectTab: Tabs = .團體
    @StateObject var vm = ViewModel()

    var body: some View {
        NavigationStack {
            VStack(spacing: 0) {
                NavigationTitle()
                TopTabbar(selectTab: $selectTab)
                    .overlay {
                        if let collectionViewBound = vm.collectionView?.bounds.width {
                            GeometryReader { geo in
                                let width = geo.size.width
                                let tabCounts: CGFloat = 4
                                let underlineWidth = width / tabCounts
                                let progress = vm.offset / collectionViewBound
                                
                                TopTabbar(selectTab: $selectTab)
                                    .overlay {
                                        Rectangle()
                                            .foregroundStyle(.red)
                                            .padding(.horizontal)
                                            .allowsHitTesting(false)
                                    }
                                    .mask(alignment: .bottomLeading) {
                                        Rectangle()
                                            .frame(width: underlineWidth, height: 2)
                                            .offset(x: progress * underlineWidth)
                                    }
                            }
                        }
                    }
                
                TabView(selection: $selectTab) {
                    Group {
                        VStack {
                            Image(systemName: "1.circle.fill")
                                .resizable()
                                .frame(width: 100, height: 100)
                                .foregroundStyle(Color(.red))
                        }
                        .tag(Tabs.團體)
                        
                        VStack {
                            Image(systemName: "2.circle.fill")
                                .resizable()
                                .frame(width: 100, height: 100)
                                .foregroundStyle(Color(.red))
                        }
                        .tag(Tabs.訂房)
                        
                        VStack {
                            Image(systemName: "3.circle.fill")
                                .resizable()
                                .frame(width: 100, height: 100)
                                .foregroundStyle(Color(.red))
                        }
                        .tag(Tabs.自由行)
                        
                        VStack {
                            Image(systemName: "4.circle.fill")
                                .resizable()
                                .frame(width: 100, height: 100)
                                .foregroundStyle(Color(.red))
                        }
                        .tag(Tabs.票券)
                    }
                    .background {
                        if !vm.isObserved {
                            // 查找並觀察 CollectionView 的偏移量
                            FindCollectionView {
                                vm.collectionView = $0
                                vm.observe()
                            }
                        }
                    }
                }
                .tabViewStyle(.page(indexDisplayMode: .never))
                .animation(.easeOut(duration: 0.2), value: selectTab)
                .background(.white)
            }
        }
    }
}

#Preview {
    MainPage()
}
