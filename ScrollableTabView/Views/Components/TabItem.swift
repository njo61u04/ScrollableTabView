//
//  TabItem.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/30.
//

import SwiftUI

struct TabItem: View {
    let text: String
    
    var body: some View {
        HStack {
            Text(text)
                .fixedSize(horizontal: true, vertical: false)
        }
    }
}

#Preview {
    TabItem(text: "Yeah!")
}
