//
//  ContentView.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/28.
//

import SwiftUI

struct TopTabbar: View {
    @Binding var selectTab: Tabs
    
    var body: some View {
        HStack {
            TabItem(text: "團體(0)")
                .frame(maxWidth: .infinity)
                .onTapGesture {
                    withAnimation(.snappy(duration: 0.3, extraBounce: 0)) {
                        selectTab = .團體
                    }
                }
            TabItem(text: "訂房(0)")
                .frame(maxWidth: .infinity)
                .onTapGesture {
                    withAnimation(.snappy(duration: 0.3, extraBounce: 0)) {
                        selectTab = .訂房
                    }
                }
            TabItem(text: "自由行(0)")
                .frame(maxWidth: .infinity)
                .onTapGesture {
                    withAnimation(.snappy(duration: 0.3, extraBounce: 0)) {
                        selectTab = .自由行
                    }
                }
            TabItem(text: "票券(0)")
                .frame(maxWidth: .infinity)
                .onTapGesture {
                    withAnimation(.snappy(duration: 0.3, extraBounce: 0)) {
                        selectTab = .票券
                    }
                }
        }
        .padding()
        .background(Color(.whiteFAFAFA)) 
    }
}

#Preview {
    TopTabbar(selectTab: .constant(.團體))
}
