//
//  NavigationTitle.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/30.
//

import SwiftUI

struct NavigationTitle: View {
    var body: some View {
        HStack {
            Spacer()
            Text("收藏")
                .font(.title2)
                .foregroundStyle(.white)
                .padding(.vertical, 8)
            Spacer()
        }
        .background(Color(.lionRed))
    }
}

#Preview {
    NavigationTitle()
}
