//
//  FindCollectionView.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/31.
//

import SwiftUI

// UIViewRepresentable 實現，用於在 SwiftUI 中查找 CollectionView
struct FindCollectionView: UIViewRepresentable {
    var result: (UICollectionView) -> ()
    
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        view.backgroundColor = .clear
        
        // 在下一次運行循環中查找 CollectionView
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            if let collectionView = view.collectionView {
                result(collectionView)
            }
        }
        
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {}
}

