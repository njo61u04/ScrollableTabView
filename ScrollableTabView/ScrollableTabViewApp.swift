//
//  ScrollableTabView.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/28.
//

import SwiftUI

@main
struct ScrollableTabViewApp: App {
    var body: some Scene {
        WindowGroup {
            MainPage()
        }
    }
}
