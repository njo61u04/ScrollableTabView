//
//  UIView+extension.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/31.
//

import UIKit

// UIView 擴展，用於查找父視圖中的 CollectionView
extension UIView {
    var collectionView: UICollectionView? {
        if let collectionView = superview as? UICollectionView {
            return collectionView
        }
        return superview?.collectionView
    }
}
