//
//  File.swift
//  ScrollableTabView
//
//  Created by Lionuser on 2024/5/28.
//

import Foundation

struct Model: Identifiable {
    var id: UUID
    var name: String
}
